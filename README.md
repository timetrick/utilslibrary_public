README
======

    
    Copyright 2015 Lennox Corporation
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
    

**Introduction** This repo contains all the strings for my Utils Library

    These are for application non-specific helpers and dialogs

    The files are all hardlinked to the main UtilsLibrary repository, in order
    to keep all translations automatically updated.

